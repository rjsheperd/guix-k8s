Guix on Kubernetes
==================

This project contains a set of manifest files for deploying [Guix](https://guix.gnu.org/) on a [Kubernetes](https://kubernetes.io/) cluster.
Familiarity with both is a prerequisite for making effective use of these resources.

Configuration
-------------

There are several settings which may be useful to adjust.

### Namespace ###

The Kubernetes namespace is defined in [`00-guix-namespace.yaml`](00-guix-namespace.yaml) and defaults to `guix-gitlab-runner` for the
intended use case of providing a Guix service for a group or project-specific [GitLab Runner](https://gitlab.com/singularsyntax/guix-gitlab-runner).

### Storage Capacity ###

Storage capacity for the Guix state `/var/guix` and store `/gnu/store` volumes are defined in [`01-guix-storage.yaml`](01-guix-storage.yaml). They
default to 1 GB and 10 GB, respectively.

### Substitutes ###

Use of pre-built [substitutes](https://guix.gnu.org/manual/en/html_node/Substitutes.html) is enabled for Guix by default. To disable, remove the
`lifecycle.postStart` section in [`03-guix-daemon.yaml`](03-guix-daemon.yaml).

Installation
------------

To install on a cluster, execute the following command - `namespace` should be the same as defined in [`00-guix-namespace.yaml`](00-guix-namespace.yaml):

    kubectl apply -f . -n <namespace>

Usage
-----

The manifests creates a Kubernetes service `guix-daemon` which is registered with the standard [service discovery](https://kubernetes.io/docs/concepts/services-networking/service/#discovering-services)
facilities.

In addition, to run `guix` commands against the Guix daemon for containerized workloads, it may be helpful to set the [`GUIX_DAEMON_SOCKET`](https://guix.gnu.org/manual/en/html_node/The-Store.html)
environment variable to `guix://guix-daemon` on pod specs as follows:

    apiVersion: batch/v1
    kind: Job
    metadata:
      name: guix-workload-job
    spec:
      template:
        spec:
          containers:
          - name: guix-workload
            image: singularsyntax/guix-client:1.1.0
            env:
            - name: GUIX_DAEMON_SOCKET
              value: "guix://guix-daemon"
